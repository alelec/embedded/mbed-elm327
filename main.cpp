/*
mbed elm327
*/

#define MBED_CONF_TARGET_STDIO_UART_TX 0
#define MBED_CONF_TARGET_STDIO_UART_RX 0

#include <mbed.h>
#include "elm.h"
#include "ReflectedSerial.h"

DigitalOut led3 (LED3);

// Serial pc (USBTX, USBRX, 115200);

const uint32_t baud = 38400 ; // 38400; // 9600

#if TARGET_NUCLEO_F103RB
#define bt_rx PA_3
#define bt_tx PA_2
#define elm_rx PC_11
#define elm_tx PC_10
#else
#define bt_rx D0
#define bt_tx D1
#define elm_rx A2
#define elm_tx A7

#endif

ReflectedSerial bt_uart (bt_tx, bt_rx, baud); 
ReflectedSerial elm_uart (elm_tx, elm_rx, baud); 
// ELM elm(&elm_uart);


// void callback_ex() {
//     while (bt_uart.readable())
//     {
//         elm_uart.putc(bt_uart.getc());
//     }
//     while (elm_uart.readable())
//     {
//         bt_uart.putc(elm_uart.getc());
//     }
// }


int main() { 

    elm_uart.forward_rx(&bt_uart);
    bt_uart.forward_rx(&elm_uart);
    // elm_uart.attach(&callback_ex);
    // bt_uart.attach(&callback_ex);

    const char* raw ;
    while (1) {
        // volatile bool is_reset = elm.reset();

        // elm.init();
        // elm.protocol(ISO_14230_4_KWP_fast_init_10_4_kbaud);

        // // elm.get_available_pids();

        // auto raw_dtcs = elm.get_dtc();
        // volatile bool is_cleared = elm.clear_dtc();

        // // pc.printf(raw_dtcs.c_str());
        
        // wait(1);
    }

}


extern "C" {
void mbed_die(void) {

    __BKPT(0);

#if !defined (NRF51_H) && !defined(TARGET_EFM32)
    core_util_critical_section_enter();
#endif
    gpio_t led_err; gpio_init_out(&led_err, LED1);    

    while (1) {
        for (int i = 0; i < 4; ++i) {
            gpio_write(&led_err, 1);
            wait_ms(150);
            gpio_write(&led_err, 0);
            wait_ms(150);
        }

        for (int i = 0; i < 4; ++i) {
            gpio_write(&led_err, 1);
            wait_ms(400);
            gpio_write(&led_err, 0);
            wait_ms(400);
        }
    }
}
}
